from django.apps import AppConfig


class YoutubeConfig(AppConfig):
    name = 'youtube'

    def ready(self):
        """
        Scheduler function, to perfrom async operations
        :return: None
        """
        from youtube.scheduler import YoutubeScheduler
        YoutubeScheduler().start_scheduler()
