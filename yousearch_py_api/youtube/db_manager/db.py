import re

import pymysql.cursors
from pymysql.constants import CLIENT

from yousearch_py_api.settings import SECRETS
from youtube import constants
from youtube.db_manager import queries


class YousearchDB:
    """
    Class to perform the database operations in MySQL
    """

    def __init__(self):
        self.connection = self.get_connection()
        self.run_migrations()

    def close(self):
        """
        Close database connection
        :return: None
        """
        self.connection.close()

    def clean_query(self, text):
        """
        Clean text of any illegal characters which might throw in MySQL syntax
        :param text: string
        :return: string
        """
        text = re.sub(r"'", r"\'", text)
        text = re.sub(r"_", r"\_", text)
        return text

    def check_if_db_exists(self, connection):
        """
        Check if provided database name (in env variables) exists?
        :param connection: DB connection object
        :return: Boolean
        """
        with connection.cursor() as cursor:
            cursor.execute(queries.SHOW_DB.format(SECRETS['DB_CREDS']['DB_NAME']))
            result = cursor.fetchone()
            cursor.close()
        return True if result else False

    def run_migrations(self):
        """
        Create required tables into database (if not exists)
        :return: None
        """
        with self.connection.cursor() as cursor:
            cursor.execute(queries.CREATE_VIDEO_TABLE)
            cursor.execute(queries.CREATE_LIST_LOG_TABLE)
            cursor.close()

    def get_connection(self):
        """
        Initiate the database connection
        This also checks if schema name exists or not? if not then, create required schema
        :return: connection object
        """
        connection = pymysql.connect(
            host=SECRETS['DB_CREDS']['DB_HOST'],
            user=SECRETS['DB_CREDS']['DB_USER'],
            port=SECRETS['DB_CREDS']['DB_PORT'],
            password=SECRETS['DB_CREDS']['DB_PASSWORD'],
            charset='utf8mb4',
            cursorclass=pymysql.cursors.DictCursor,
            client_flag=CLIENT.MULTI_STATEMENTS
        )
        if self.check_if_db_exists(connection):
            connection.select_db(SECRETS['DB_CREDS']['DB_NAME'])
        else:
            connection.cursor().execute(queries.CREATE_DB.format(SECRETS['DB_CREDS']['DB_NAME']))
            connection.select_db(SECRETS['DB_CREDS']['DB_NAME'])
        return connection

    def store_next_page_token(self, next_page_token):
        """
        Store next page token into database to keep log of next page and avail pagination facility on db level
        :param next_page_token: string
        :return: None
        """
        with self.connection.cursor() as cursor:
            cursor.execute(queries.UPDATE_LIST_LOG.format(next_page_token))
            self.connection.commit()
            cursor.close()

    def fetch_next_page_token(self):
        """
        Fetch next page token from database to return updated results
        :return: string / None
        """
        with self.connection.cursor() as cursor:
            cursor.execute(queries.FETCH_NEXT_TOKEN)
            res = cursor.fetchone()
            cursor.close()
        return res.get(constants.NEXT_PAGE_TOKEN) if res else None

    def search_vid(self, keyword, res_count, order=0):
        """
        Search video from database and return them in particular order
        :param keyword: string, regex string containing all the keywords
        :param res_count: int, count of result items to return
        :param order: int, order = 0 or odd means decreasing, even means increasing
        :return: tuple(list, int), (items returned, total count of records found in db)
        """
        if order != 0 and order is not None:
            seq = 'ORDER BY publishTime ASC' if (int(order % 2) == 0) else 'ORDER BY publishTime DESC'
        else:
            seq = ''
        if keyword:
            with self.connection.cursor() as cursor:
                cursor.execute(queries.SEARCH_VID.format(keyword, keyword, seq, res_count))
                result = cursor.fetchall()
                cursor.execute(queries.COUNT_SEARCH_VID.format(keyword, keyword, res_count))
                count = cursor.fetchone().get(constants.TOTAL, 0)
                cursor.close()
            if isinstance(result, dict):
                result = [result]
            return (result, count) if result else ({}, count)
        else:
            with self.connection.cursor() as cursor:
                cursor.execute(queries.SEARCH_ALL_VID.format(seq, res_count))
                result = cursor.fetchall()
                cursor.execute(queries.COUNT_ALL_VID)
                count = cursor.fetchone().get(constants.TOTAL, 0)
                cursor.close()
            if isinstance(result, dict):
                result = [result]
            return (result, count) if result else ([], count)

    def search_vid_by_title(self, keyword, res_count, order=0):
        """
        Search video from database using title and return them in particular order
        :param keyword: string, regex string containing all the keywords
        :param res_count: int, count of result items to return
        :param order: int, order = 0 or odd means decreasing, even means increasing
        :return: tuple(list, int), (items returned, total count of records found in db)
        """
        with self.connection.cursor() as cursor:
            cursor.execute(queries.SEARCH_VID_TITLE.format(keyword, res_count))
            result = cursor.fetchall()
            cursor.execute(queries.COUNT_VID_TITLE.format(keyword, res_count))
            count = cursor.fetchone().get(constants.TOTAL, 0)
            cursor.close()
        if isinstance(result, dict):
            result = [result]
        return (result, count) if result else ([], count)

    def search_vid_by_desc(self, keyword, res_count, order=0):
        """
        Search video from database using description and return them in particular order
        :param keyword: string, regex string containing all the keywords
        :param res_count: int, count of result items to return
        :param order: int, order = 0 or odd means decreasing, even means increasing
        :return: tuple(list, int), (items returned, total count of records found in db)
        """
        with self.connection.cursor() as cursor:
            cursor.execute(queries.SEARCH_VID_DESCRIPTION.format(keyword, res_count))
            result = cursor.fetchall()
            cursor.execute(queries.COUNT_VID_DESCRIPTION.format(keyword, res_count))
            count = cursor.fetchone().get(constants.TOTAL, 0)
            cursor.close()
        if isinstance(result, dict):
            result = [result]
        return (result, count) if result else ({}, count)

    def store_video_results(self, items):
        """
        Store videos fetched from youtube into database
        :param items: list, youtube videos details
        :return: None
        """
        q_list = list()
        for entry in items:
            query = queries.UPDATE_VIDEO.format(
                entry[constants.VIDEO_ID],
                constants.YOUTUBE_CAPS,
                re.sub(r'T', r' ', re.sub(r'Z', r'', entry[constants.PUBLISH_TIME])),
                self.clean_query(entry[constants.TITLE]),
                self.clean_query(entry[constants.THUMBNAIL]),
                self.clean_query(entry[constants.CHANNEL_TITLE]),
                self.clean_query(entry[constants.DESCRIPTION]),
                entry[constants.VIDEO_ID],
                constants.YOUTUBE_CAPS,
                re.sub(r'T', r' ', re.sub(r'Z', r'', entry[constants.PUBLISH_TIME])),
                self.clean_query(entry[constants.TITLE]),
                self.clean_query(entry[constants.THUMBNAIL]),
                self.clean_query(entry[constants.CHANNEL_TITLE]),
                self.clean_query(entry[constants.DESCRIPTION]),
            )
            query = re.sub(r'None', 'null', query)
            q_list.append(query)
        with self.connection.cursor() as cursor:
            for query in q_list:
                cursor.execute(query)
            self.connection.commit()
            cursor.close()
