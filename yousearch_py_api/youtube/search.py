import re

from youtube import constants
from youtube.db_manager.db import YousearchDB


def clean_keywords(text):
    """
    Clean search keywords for efficient database searching
    :param text: keyword text
    :return: list of filtered token used for searching
    """
    # remove all special chars
    text = re.sub(r'\W+', ' ', text)
    # remove extra spaces
    text = re.sub(r'\s+', ' ', text)
    tlist = text.split()
    # remove duplicates
    tlist = list(set(tlist))
    return tlist


def calculate_hits(keywords, text):
    """"""
    hits = 0
    for pat in keywords:
        hits += text.lower().count(pat.lower())
    return hits


def fetch_search_results(search_text='', res_count=10, title_search='', desc_search='', order=None):
    """
    Generic method to search youtube search results and order for API as well as frontend
    :param search_text: string --> search keywords
    :param res_count: int --> number of search results to return
    :param title_search: string --> title search keywords
    :param desc_search: string --> description search keywords
    :param order: integer even, 0 = ascending & odd = descending, None = order by hits
    :return: dict --> search results
    """
    title_search = title_search if title_search else ''
    desc_search = desc_search if desc_search else ''
    title_search = clean_keywords(title_search)
    desc_search = clean_keywords(desc_search)
    keywords_list = clean_keywords(search_text)
    result = list()

    # Fetch results from database
    connection = YousearchDB()
    if title_search:
        videos, total_count = connection.search_vid_by_title("|".join(title_search), res_count)
    elif desc_search:
        videos, total_count = connection.search_vid_by_desc("|".join(desc_search), res_count)
    else:
        videos, total_count = connection.search_vid("|".join(keywords_list), res_count, order)
    connection.close()

    # calculate keywords found in video
    for entry in videos:
        hits = calculate_hits(keywords_list, str(entry[constants.TITLE]) + str(entry[constants.DESCRIPTION]))
        result.append([hits, entry])

    start = res_count - 10
    start = start if start > 0 else 0

    # Sort the results in mentioned order
    if order is None:
        result = sorted(result, key=lambda x: x[0], reverse=True)[start:res_count]
    elif order == 0 or int(order % 2) != 0:
        result = sorted(result, key=lambda x: x[1][constants.PUBLISH_TIME], reverse=True)[start:res_count]
    else:
        result = sorted(result, key=lambda x: x[1][constants.PUBLISH_TIME], reverse=False)[start:res_count]
    result = [{constants.HITS: x[0], **x[1]} for x in result]

    for x in result:
        x[constants.VIDEO_LINK] = constants.YOUTUBE_LINK_FORMAT.format(x[constants.VIDEO_ID])
        x[constants.TRIMMED_TITLE] = x[constants.TITLE][:50] + ("..." if len(x[constants.TITLE]) > 50 else '')

    return {
        constants.SEARCH_QUERY: search_text,
        constants.KEYWORDS: keywords_list,
        constants.ORDER: order if order else 0,
        constants.PAGE: int(res_count / 10),
        constants.PAGE_START: start + 1,
        constants.PAGE_END: start + len(result),
        constants.TOTAL_RESULTS: total_count,
        constants.PAGE_RESULTS: len(result),
        constants.RESULTS: result
    }
