from django.conf.urls import url

from . import views

app_name = 'youtube'
urlpatterns = [
	url(r'^searchApi/$', views.search_vid, name='search_api'),
	url(r'^home/$', views.index, name='home'),
	url(r'^search/$', views.search, name='search'),
]
