import traceback

from googleapiclient.discovery import build

from yousearch_py_api.settings import SECRETS
from youtube import constants


def format_yt_response(raw_data):
    """
    Convert Raw youtube details to format for API response/output
    :param raw_data: dict
    :return: tuple(string -> next page token, list -> list of all youtube videos)
    """
    next_page_token = raw_data[constants.NEXT_PAGE_TOKEN]
    items = [
        {
            constants.VIDEO_ID: x[constants.ID][constants.VIDEO_ID],
            constants.PUBLISHED_AT: x[constants.SNIPPET][constants.PUBLISHED_AT],
            constants.TITLE: x[constants.SNIPPET][constants.TITLE],
            constants.DESCRIPTION: x[constants.SNIPPET][constants.DESCRIPTION],
            constants.THUMBNAIL: x[constants.SNIPPET][constants.THUMBNAILS][constants.DEFAULT][constants.URL],
            constants.CHANNEL_TITLE: x[constants.SNIPPET][constants.CHANNEL_TITLE],
            constants.PUBLISH_TIME: x[constants.SNIPPET][constants.PUBLISH_TIME]
        } for x in raw_data[constants.ITEMS]
    ]
    return next_page_token, items


def fetch_youtube_data(next):
    """
    Fetch latest video data from YouTube
    :param next: string -> next page token
    :return: tuple(string -> updated next page token, dict, formatted youtube data response)
    """
    next = next if next else None
    for api_key in SECRETS['API_KEYS']['YOUTUBE_API_KEYS']:
        try:
            yt = build(SECRETS['SERVICE'],
                       SECRETS['SERVICE_VERSION'],
                       developerKey=api_key)
            request = yt.search().list(
                part='snippet',
                type='video',
                order='date',
                maxResults=SECRETS['MAX_RESULT_PACKET'],
                q=SECRETS['QUERY_TOPIC'],
                pageToken=next,
                publishedAfter=SECRETS['FETCH_YT_PUBLISHED_AFTER']
            )
            response = request.execute()
            next_page_token, items = format_yt_response(response)
            return (next_page_token, items) if items else (None, None)
        except Exception as e:
            traceback.print_exc()
    raise Exception('YouTube API data fetching failed')
