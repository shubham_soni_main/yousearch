import math
import traceback

from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_200_OK

from youtube import constants
from youtube.search import fetch_search_results


@api_view(['GET'])
def search_vid(request):
    """ Search video details using API """
    try:
        text = request.GET.get(constants.SEARCH_QUERY, '')
        res_count = int(request.GET.get(constants.PAGE, 1))
        title_search = request.GET.get(constants.TITLE_QUERY, None)
        desc_search = request.GET.get(constants.DESCRIPTION_QUERY, None)
        results = fetch_search_results(text, int(res_count) * 10, title_search, desc_search)
        return Response(results, status=HTTP_200_OK)
    except Exception as e:
        traceback.print_exc()
        return Response(status=HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['GET'])
def index(request):
    """ Render Home page template """
    try:
        results = fetch_search_results()
        return render(request, 'index.html', results)
    except Exception as e:
        traceback.print_exc()

@api_view(['GET', 'POST'])
def search(request):
    """ Search API to render the template """
    if request.method == 'GET':
        max_res = int(request.GET.get(constants.MAX, 0))
        order = request.GET.get(constants.ORDER, 0)
        order = int(order) if order else 0
        text = request.GET.get(constants.SEARCH_QUERY, '')
        res_count = int(request.GET.get(constants.PAGE, 1))

        if res_count < 1:
            res_count = 1
        elif res_count > math.ceil(max_res / 10):
            res_count = math.ceil(max_res / 10)

        title_search = request.GET.get(constants.TITLE_QUERY, None)
        desc_search = request.GET.get(constants.DESCRIPTION_QUERY, None)
    else:
        order = int(request.POST.get(constants.ORDER, 0))
        text = request.POST.get(constants.SEARCH_QUERY, '')
        res_count = int(request.POST.get(constants.PAGE, 1))
        title_search = request.POST.get(constants.TITLE_QUERY, None)
        desc_search = request.POST.get(constants.DESCRIPTION_QUERY, None)

    results = fetch_search_results(text, int(res_count) * 10, title_search, desc_search, order=order)
    return render(request, 'index.html', results)
